import { Module, HttpModule } from '@nestjs/common';
import { DevicesController } from './devices.controller';
import { DevicesService } from './devices.service';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    HttpModule
  ],
  controllers: [DevicesController],
  providers: [DevicesService]
})
export class DevicesModule { }
