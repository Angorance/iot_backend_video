import { Controller, UseGuards, Post, Body, Get, Param, Patch, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { DevicesService } from './devices.service';
import { NewDeviceDTO } from './dto/new-device.dto';
import { DeviceParametersDTO } from './dto/device-parameters.dto';
import { PublicKeyDTO } from './dto/public-key.dto';
import * as swagger from '@nestjs/swagger';

@swagger.ApiUseTags('Devices')
@swagger.ApiBearerAuth()
@Controller('devices')
@UseGuards(AuthGuard())
export class DevicesController {

  constructor(private readonly devicesService: DevicesService) { }

  @Post()
  @swagger.ApiConsumes('application/json')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async addDevice(@Body() newDevice: NewDeviceDTO): Promise<PublicKeyDTO> {
    return await this.devicesService.addDevice(newDevice);
  }

  @Get('next/:deviceIp')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiOkResponse({ description: "Ok" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  nextParingStep(@Param('deviceIp') deviceIp: string) {
    return this.devicesService.pingDevice(deviceIp);
  }

  @Delete('/:deviceId')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async removeDevice(@Param('deviceId') deviceId: string) {
    return await this.devicesService.removeDevice(deviceId);
  }

  @Get()
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async listDevices() {
    return await this.devicesService.listDevices();
  }

  @Patch('params/:deviceId')
  @swagger.ApiConsumes('application/json')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  updateParams(@Param() deviceId: string, @Body() params: DeviceParametersDTO) {
    return this.devicesService.updateParams(deviceId, params);
  }

  @Get('clip/:deviceId')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  toggleVideo(@Param() deviceId: string) {
    return this.devicesService.toggleVideo(deviceId);
  }
}
