import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class DeviceParametersDTO {

  constructor(start: number, duration: number) {
    this.start = start;
    this.duration = duration;
  }

  @ApiModelProperty()
  @IsNotEmpty()
  @IsNumber()
  readonly start: number;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsNumber()
  readonly duration: number;
}
