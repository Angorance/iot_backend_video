import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsIP } from 'class-validator';

export class NewDeviceDTO {

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsIP()
  ip: string;

  @ApiModelProperty()
  @IsNotEmpty()
  publicKey: string;
}
