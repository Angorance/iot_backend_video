import { ApiModelProperty } from '@nestjs/swagger';

export class PublicKeyDTO {

  constructor(publicKey: string) {
    this.publicKey = publicKey;
  }

  @ApiModelProperty()
  readonly publicKey: string;
}
