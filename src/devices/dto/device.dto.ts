import { ApiModelProperty } from '@nestjs/swagger';
import { NewDeviceDTO } from './new-device.dto';

export class DeviceDTO {

  constructor(newDevice: NewDeviceDTO) {
    this.name = newDevice.name;
    this.description = newDevice.description;
    this.ip = newDevice.ip;
    this.publicKey = newDevice.publicKey;

    this.startParam = Number.parseInt(process.env.START_PARAM);
    this.durationParam = Number.parseInt(process.env.DURATION_PARAM);
  }

  @ApiModelProperty()
  name: string;

  @ApiModelProperty()
  description: string;

  @ApiModelProperty()
  ip: string;

  @ApiModelProperty()
  publicKey: string;

  @ApiModelProperty()
  startParam: number;

  @ApiModelProperty()
  durationParam: number;
}
