import { HttpException, HttpStatus, Injectable, HttpService, NotImplementedException } from '@nestjs/common';
import { NewDeviceDTO } from './dto/new-device.dto';
import { DeviceParametersDTO } from './dto/device-parameters.dto';
import { DeviceDTO } from './dto/device.dto';
import { PublicKeyDTO } from './dto/public-key.dto';
import { DbQueryHandler } from '../database/dbQueryHandler';

import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';
import * as sodium from 'libsodium-wrappers';

const pathToKey = path.join(os.homedir(), process.env.KEY_PATH);
const PUB = path.join(pathToKey, process.env.PUBLIC_KEY);
const PRI = path.join(pathToKey, process.env.PRIVATE_KEY);

// FIXME: Endpoints inexistant on devices!

const start_param: number = Number.parseInt(process.env.START_PARAM);
const duration_param: number = Number.parseInt(process.env.DURATION);

@Injectable()
export class DevicesService {

  private readonly dbQueryHandler: DbQueryHandler = DbQueryHandler.getInstance();

  constructor(private readonly httpService: HttpService) { }

  async addDevice(newDevice: NewDeviceDTO): Promise<PublicKeyDTO> {
    const device: DeviceDTO = new DeviceDTO(newDevice);
    const res = await this.dbQueryHandler.addDevice(device);

    if (!res) {
      console.log('Unable to add device');

      throw new HttpException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
      }, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    // Recovers the public key
    const publicKey: PublicKeyDTO = new PublicKeyDTO(fs.readFileSync(PUB, 'utf8'));

    return publicKey;
  }

  async pingDevice(deviceIp: string) {
    await sodium.ready;

    // Prepare the payload to send with device parameters
    const params: DeviceParametersDTO = new DeviceParametersDTO(start_param, duration_param);
    const payload = { parameters: params };
    let privateKey = fs.readFileSync(PRI);

    privateKey = sodium.from_base64(privateKey);

    // Sign the payload with the private key
    let payload_signed = sodium.crypto_sign(sodium.from_string(payload), privateKey);
    //let url = 'http://' + deviceIp + '/pairingPayload'; FIXME: MODIFY

    // Send the payload to the device
    //let ping = this.httpService.post(url, { body: payload_signed }); TODO: NOT IMPLEMENTED ON DEVICES

    return payload_signed;
  }

  async removeDevice(deviceId: string) {
    return await this.dbQueryHandler.deleteDevice(deviceId);
  }

  async listDevices() {
    return await this.dbQueryHandler.getDevices();
  }

  // Not implemented on device side...
  updateParams(deviceId: string, params: DeviceParametersDTO) {
    // TODO: Update params for device
    // TODO: Send new parameters to device
    return NotImplementedException;
  }

  // Not implemented on device side...
  toggleVideo(deviceId: string) {
    // TODO: Request device to send videoclip to save
    return NotImplementedException;
  }
}
