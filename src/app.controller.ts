import { Controller, Get, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import * as swagger from '@nestjs/swagger';

@Controller()
@swagger.ApiBearerAuth()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('validation')
  @swagger.ApiConsumes('plain/text')
  @swagger.ApiProduces('plein/text')
  @UseGuards(AuthGuard())
  validate() { }
}
