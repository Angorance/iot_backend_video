import { Body, Controller, Injectable, Post } from '@nestjs/common';
import * as swagger from '@nestjs/swagger';
import { AccountsService } from './accounts.service';
import { Token } from './dto/token.dto';
import { Auth } from './dto/auth.dto';
import { NewAccountDTO } from './dto/new-account.dto';

@Controller('accounts')
@swagger.ApiUseTags('Account')
@Injectable()
export class AccountsController {

  constructor(private readonly accountsService: AccountsService) { }

  /**
   * Endpoint: POST accounts/registration
   * allow to insert a new user in the database if the mail/username doesn't exist
   * @param newUser User to register, JSON format
   */
  @Post('registration')
  @swagger.ApiConsumes('application/json')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiForbiddenResponse({ description: "Already exists" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async Registration(@Body() newUser: NewAccountDTO) {
    return this.accountsService.registration(newUser);
  }

  /**
   * Endpoint: POST accounts/authentication
   * Check the user credential, if a match is found in the database, return a token
   * @param loginReq user credentials for a login attempt, JSON format
   */
  @Post('authentication')
  @swagger.ApiConsumes('application/json')
  @swagger.ApiProduces('application/json')
  @swagger.ApiCreatedResponse({ description: 'Authentication ok', type: Token })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiForbiddenResponse({ description: "Bad Credentials" })
  async Authentication(@Body() loginReq: Auth) {
    return this.accountsService.authentication(loginReq);
  }
}
