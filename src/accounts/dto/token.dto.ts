import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

/**
 * Credentials DTO for authentication.
 */
export class Token {

  constructor(token: string) {
    this.token = token;
  }

  @ApiModelProperty()
  @IsNotEmpty()
  readonly token: string;
}