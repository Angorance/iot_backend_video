import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class NewAccountDTO {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  readonly username: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
