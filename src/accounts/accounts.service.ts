import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { DbQueryHandler } from '../database/dbQueryHandler';
import { UserModel } from '../interfaces/models/user.model';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { Token } from './dto/token.dto';
import { NewAccountDTO } from './dto/new-account.dto';
import { Auth } from './dto/auth.dto';

@Injectable()
export class AccountsService {
  private readonly dbQueryHandler: DbQueryHandler = DbQueryHandler.getInstance();

  constructor(private readonly jwtService: JwtService) { }

  async registration(newUser: NewAccountDTO) {
    console.log('Registration :' + JSON.stringify(newUser));

    // Check if user exists
    const userExsists = await this.dbQueryHandler.isEmailAvailable(newUser.email);
    console.log(userExsists);

    if (!userExsists) {
      console.log('user already exists');

      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: "Already exists",
      }, 403);
    }

    // If doesn't exists insert new user in database
    const userInserted = await this.dbQueryHandler.addUser(newUser);

    if (!userInserted) {
      console.log('couldn\'t register the user');

      throw new HttpException({
        status: HttpStatus.INTERNAL_SERVER_ERROR,
        error: "Can't create user",
      }, 500);
    } else {
      console.log('New user created');

      return "Ok";
    }
  }

  async authentication(loginReq: Auth) {
    console.log('login attempt: ' + JSON.stringify(loginReq));

    // retrieve the user ID
    const userInfo: UserModel = await this.dbQueryHandler.getUserByLogin(loginReq.email, loginReq.password);

    if (userInfo === null) {
      console.log('bad credentials');

      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: "Bad Credentials",
      }, 403);
    }

    // token generation
    const payload: JwtPayload = { id: userInfo.id, email: userInfo.email };
    const token: Token = new Token(this.jwtService.sign(payload));

    return token;
  }
}
