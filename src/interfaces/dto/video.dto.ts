import {ApiModelProperty} from '@nestjs/swagger';

export class Video {
    @ApiModelProperty()
    readonly fieldname: string;

    @ApiModelProperty()
    readonly originalname: string;

    @ApiModelProperty()
    readonly encoding: string;

    @ApiModelProperty()
    readonly mimetype: string;

    @ApiModelProperty()
    readonly destination: string;

    @ApiModelProperty()
    readonly filename: string;

    @ApiModelProperty()
    readonly path: string;

    @ApiModelProperty()
    readonly size: number;
}
