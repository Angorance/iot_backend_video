import {IsEmail} from 'class-validator';
import {Column as ColumnORM, Entity, ObjectIdColumn} from 'typeorm';

@Entity( {name: 'person'} )
export class UserModel {
    @ObjectIdColumn( {readonly: true} )
    id: string;

    @ColumnORM( {readonly: true} )
    @IsEmail()
    email: string;

    @ColumnORM()
    password: string;
}
