import { Column as ColumnORM, Entity, ObjectIdColumn } from 'typeorm';

@Entity({ name: 'device' })
export class DeviceModel {
  @ObjectIdColumn({ readonly: true })
  id: string;

  @ColumnORM()
  ip: string;

  @ColumnORM()
  name: string;

  @ColumnORM()
  description: string;

  @ColumnORM()
  publicKey: string;

  @ColumnORM()
  startParam: string;

  @ColumnORM()
  durationParam: string;
}
