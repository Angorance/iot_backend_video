import * as mongoose from 'mongoose';

export const VideosModel = new mongoose.Schema( {
    id: String,
    fieldname: String,
    originalname: String,
    encoding: String,
    mimetype: String,
    destination: String,
    filename: String,
    path: String,
    size: Number,
} );
