import * as bcrypt from 'bcryptjs';
import { deserialize } from 'class-transformer';
import { getMongoRepository, MongoRepository } from 'typeorm';
import { UserModel } from '../interfaces/models/user.model';
import { validate } from 'class-validator';
import { NewAccountDTO } from 'src/accounts/dto/new-account.dto';
import { DeviceModel } from '../interfaces/models/device.model';
import { DeviceDTO } from 'src/devices/dto/device.dto';

export class DbQueryHandler {
  private static _instance: DbQueryHandler;

  private readonly userRepository: MongoRepository<UserModel> = getMongoRepository(UserModel);
  private readonly deviceRepository: MongoRepository<DeviceModel> = getMongoRepository(DeviceModel);

  private constructor() {
    if (DbQueryHandler._instance) {
      throw new Error('Error: Instantiation failed: Use DbQueryHandler.getInstance() instead of new.');
    }
    DbQueryHandler._instance = this;
  }

  public static getInstance(): DbQueryHandler {
    if (!DbQueryHandler._instance) {
      DbQueryHandler._instance = new DbQueryHandler();
    }
    return DbQueryHandler._instance;
  }

  public async getUsers({ limit = 50, page = 0 } = {}): Promise<UserModel[]> {
    let skip;
    if (page !== 0) {
      skip = page * limit;
    }

    const users = await this.userRepository.find();
    return users; // .sort({}).skip(+skip).limit(+limit);
  }

  public getUserById(id: string): Promise<UserModel> {
    return this.userRepository.findOne(id);
  }

  public async getUserByLogin(email: string, password: string): Promise<UserModel> {
    const user = await this.userRepository.findOne({ email });
    if (user == null || user.id == null) {
      throw new Error(`There is no user corresponding to the email "${email}"`);
    }

    // un user avec cet email a été trouvée dans la DB -> on compare le password reçu en paramètre avec le mdp enregistré dans la DB
    const match = await bcrypt.compare(password, user.password);
    if (!match) {
      throw new Error('Received password is not correct!');
    }
    // si le mdp est correct, on retourne la userne{}
    return user;
  }

  public async isEmailAvailable(email: string): Promise<boolean> {
    const existingUser = await this.userRepository.findOne({ email });
    return existingUser == null;
  }

  public async addUser(newUser: NewAccountDTO): Promise<UserModel> {
    const json = JSON.stringify(newUser);
    const user: UserModel = deserialize(UserModel, json);

    await this.validateUser(user);
    user.password = bcrypt.hashSync(newUser.password, 10);
    return this.userRepository.save(user);
  }

  private async validateUser(user: UserModel): Promise<void> {
    const validationErrors = await validate(user);
    if (validationErrors.length > 0) {
      throw validationErrors;
    }
  }

  /**
   * Add a device into the database
   * 
   * @param newDevice New device to add
   */
  public async addDevice(newDevice: DeviceDTO): Promise<DeviceModel> {
    const json = JSON.stringify(newDevice);
    const device: DeviceModel = deserialize(DeviceModel, json);

    return this.deviceRepository.save(device);
  }

  /**
   * Delete the given device from the database
   */
  public async deleteDevice(deviceId: string): Promise<DeviceModel> {
    const device: DeviceModel = await this.deviceRepository.findOne(deviceId);
    const deleted = this.deviceRepository.remove(device);

    return deleted;
  }

  /**
   * Get all devices from the database
   */
  public async getDevices(): Promise<DeviceModel[]> {
    const devices = await this.deviceRepository.find();

    return devices;
  }
}
