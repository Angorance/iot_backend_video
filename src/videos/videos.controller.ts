import { Controller, Delete, Get, Param, Post, UploadedFile, UseInterceptors, Res, UseGuards } from '@nestjs/common';
import { VideosService } from './videos.service';
import * as swagger from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { ObjectID } from 'bson';
import { Video } from '../interfaces/dto/video.dto';
import { AuthGuard } from '@nestjs/passport';

@swagger.ApiUseTags('Videos')
@swagger.ApiBearerAuth()
@Controller('videos')
@UseGuards(AuthGuard())
export class VideosController {
  constructor(private readonly videosService: VideosService) { }

  /**
   * Endpoint: POST videos/
   * Upload a video on the server
   * @param file the file to download
   */
  @Post()
  @swagger.ApiConsumes('multipart/form-data')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiCreatedResponse({ description: "Created" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  @swagger.ApiImplicitFile({ name: 'file', required: true, description: 'Video' })
  @UseInterceptors(FileInterceptor('file', { dest: './videos' }))
  async uploadVideo(@UploadedFile() file: Video) {
    return this.videosService.uploadVideo(file);
  }

  /**
   * Endpoint: Get videos/
   * Get the information of all the videos stored on the server
   */
  @Get()
  @swagger.ApiProduces('application/json')
  @swagger.ApiOkResponse({ description: "Ok" })
  @swagger.ApiForbiddenResponse({ description: "Forbidden" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async getStoredVideosInfos() {
    return this.videosService.getStoredVideosInfos();
  }

  /**
   * Endpoint: Get videos/:id
   * Get the information of the videos corresponding to the received id and stored on the server
   */
  @Get(':id')
  @swagger.ApiProduces('application/json')
  @swagger.ApiOkResponse({ description: "Ok" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiForbiddenResponse({ description: "Forbidden" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async getStoredVideoInfo(@Param('id') id: string) {
    return this.videosService.getStoredVideosInfos({ _id: new ObjectID(id) });
  }

  /**
   * Endpoint: Get videos/:id/download/
   * Get all the videos stored on the server
   */
  @Get(':id/download')
  @swagger.ApiProduces('application/json')
  @swagger.ApiOkResponse({ description: "Ok" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiForbiddenResponse({ description: "Forbidden" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server error" })
  async downloadVideo(@Param('id') id: string, @Res() res) {
    return this.videosService.downloadVideo(new ObjectID(id), res);
  }

  /**
   * Endpoint: Delete videos/:id/
   * Delete the video corresponding to the received id from the server storage
   */
  @Delete(':id')
  @swagger.ApiProduces('plain/text')
  @swagger.ApiOkResponse({ description: "Deleted" })
  @swagger.ApiBadRequestResponse({ description: "Bad Request" })
  @swagger.ApiForbiddenResponse({ description: "Forbidden" })
  @swagger.ApiInternalServerErrorResponse({ description: "Internal Server Error" })
  async deleteVideo(@Param('id') id: string) {
    return this.videosService.deleteVideo(new ObjectID(id));
  }
}
