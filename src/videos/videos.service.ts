import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ObjectID } from 'mongodb';
import { Video } from '../interfaces/dto/video.dto';
import { Video as VideoInterface } from '../interfaces/video.interface';
import * as fs from 'fs';

@Injectable()
export class VideosService {
  constructor(@Inject('VIDEO_MODEL') private readonly videoModel: Model<VideoInterface>) {
  }

  public async uploadVideo(uploadedFile: Video): Promise<Video> {
    return await new this.videoModel(uploadedFile).save();
  }

  public async getStoredVideosInfos(filter = {}): Promise<Video[]> {
    return await this.videoModel.find(filter);
  }

  public async downloadVideo(id: ObjectID, res) {
    const video: VideoInterface = await this.videoModel.findById(id);

    return res.download(process.env.INIT_CWD + '/' + video.path, video.originalname);
  }

  public async deleteVideo(id: ObjectID) {
    const video = await this.videoModel.findByIdAndRemove(id);
    await fs.unlink(video.path, (err) => {
      if (err) {
        console.log('failed to delete local image:' + err);
      } else {
        console.log('successfully deleted local image');
      }
    },
    );
    return video;
  }
}
