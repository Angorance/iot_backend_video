import { Module } from '@nestjs/common';
import { VideosController } from './videos.controller';
import { VideosService } from './videos.service';
import { DatabaseModule } from '../database/database.module';
import { videosProviders } from './videos.providers';
import { PassportModule } from '@nestjs/passport';

@Module({
  imports: [DatabaseModule, PassportModule.register({ defaultStrategy: 'jwt' }),],
  controllers: [VideosController],
  providers: [VideosService, ...videosProviders],
})
export class VideosModule {

}
