import { Connection } from 'mongoose';
import { VideosModel } from '../interfaces/models/videos.model';

export const videosProviders = [
  {
    provide: 'VIDEO_MODEL',
    useFactory: (connection: Connection) => connection.model('uploads', VideosModel),
    inject: ['DATABASE_CONNECTION'],
  },
];
