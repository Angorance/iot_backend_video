import * as Dotenv from 'dotenv';
import {ConnectionOptions} from 'typeorm';

Dotenv.config();

interface Configuration {
    HOST: string;
    PORT: number;
    DB_PORT: number;
    DB_NAME: string;
    URL: string;
    JWT_SECRET: string;
    LOGGING: boolean;
}

export class Config {
    static readonly env: string = process.env.NODE_ENV || 'development';

    static readonly DEFAULT_PORT: number = 3000;
    static readonly DEFAULT_DB_PORT: number = 27017;

    private static readonly configs: { [key: string]: Configuration } = {
        development: {
            HOST: process.env.MONGODB_HOST_DEV,
            PORT: + process.env.PORT || Config.DEFAULT_PORT,
            DB_PORT: + process.env.MONGODB_PORT_DEV || Config.DEFAULT_DB_PORT,
            DB_NAME: process.env.MONGODB_DBNAME_DEV,
            URL: `mongodb://${process.env.MONGODB_HOST_DEV}:${process.env.MONGODB_PORT_DEV}/${process.env.MONGODB_DBNAME_DEV}`,
            JWT_SECRET: `${process.env.JWT_SECRET}`,
            LOGGING: true,
        },

        test: {
            HOST: process.env.MONGODB_HOST_TEST,
            PORT: + process.env.PORT || Config.DEFAULT_PORT,
            DB_PORT: + process.env.MONGODB_PORT_TEST || Config.DEFAULT_DB_PORT,
            DB_NAME: process.env.MONGODB_DBNAME_TEST,
            URL: `mongodb://${process.env.MONGODB_HOST_TEST}:${process.env.MONGODB_PORT_TEST}/${process.env.MONGODB_DBNAME_TEST}`,
            JWT_SECRET: `${process.env.JWT_SECRET}`,
            LOGGING: true,
        },

        production: {
            HOST: process.env.MONGODB_HOST_PROD,
            PORT: + process.env.PORT || Config.DEFAULT_PORT,
            DB_PORT: + process.env.MONGODB_PORT_PROD || Config.DEFAULT_DB_PORT,
            DB_NAME: process.env.MONGODB_DBNAME_PROD,
            URL: `mongodb+srv://${process.env.MONGODB_USER_PROD}:${process.env.MONGODB_PWD_PROD}@${process.env.MONGODB_SERVER_PROD}/${process.env.MONGODB_DBNAME_PROD}?retryWrites=true&w=majority`,
            JWT_SECRET: `${process.env.JWT_SECRET}`,
            LOGGING: false,
        },
    };

    static readonly CONFIG = Config.configs[Config.env];

    static readonly DB_CONNECTION: ConnectionOptions = {
        type: 'mongodb',
        host: Config.CONFIG.HOST,
        port: Config.CONFIG.DB_PORT,
        url: Config.CONFIG.URL,
        database: Config.CONFIG.DB_NAME,
        useNewUrlParser: true,
        appname: 'IOTBackend API',
        logging: Config.CONFIG.LOGGING,
        loggerLevel: 'error',
        entities: [ __dirname + '/interfaces/models/*.model.*' ],
    };
}
