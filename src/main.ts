import * as Dotenv from 'dotenv';
Dotenv.config();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Connection, createConnection } from 'typeorm';
import { Config } from './config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

import * as os from 'os';
import * as fs from 'fs';
import * as path from 'path';
import * as sodium from 'libsodium-wrappers';

async function bootstrap() {
  await createConnection(Config.DB_CONNECTION).then(async (connect: Connection) => {
    console.log(`Connecté à la base de donnée de ${process.env.NODE_ENV} --> ${Config.CONFIG.URL}`);
    return connect;
  }).catch((error) => {
    console.log(`Erreur de connexion à la base de donnée de ${process.env.NODE_ENV} --> ${Config.CONFIG.URL}`);
    console.log(error);
  });

  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());

  await keygen();

  const options = new DocumentBuilder()
    .setTitle('IOT Video Back-end')
    .setDescription('Specify the APIs the video back-end exposes')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  await app.listen(Config.CONFIG.PORT);
  console.log('listening on port: ' + Config.CONFIG.PORT);
}
bootstrap();

/**
 * The folder / files used or created here must survive if machine shuts down!
 */
async function keygen() {
  const pathToKey = path.join(os.homedir(), process.env.KEY_PATH);
  const PUB = path.join(pathToKey, process.env.PUBLIC_KEY);
  const PRI = path.join(pathToKey, process.env.PRIVATE_KEY);

  if (!fs.existsSync(PUB) || !fs.existsSync(PRI)) {
    console.log('No keys found, generating them...');

    await sodium.ready;

    if (!fs.existsSync(pathToKey)) {
      fs.mkdirSync(pathToKey);
    }

    const result = sodium.crypto_box_keypair();

    console.log('Keys generated');

    const k_public = sodium.to_base64(result.publicKey);
    const k_private = sodium.to_base64(result.privateKey);

    fs.writeFileSync(PUB, k_public);
    fs.writeFileSync(PRI, k_private);

    console.log('Keys saved');
  }
}
