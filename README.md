# IOT - Backend Vidéo

Auteurs: Benoît Schopfer, Daniel Gonzalez Lopez

Repo: https://gitlab.com/Angorance/iot_backend_video

## Technologies utilisées

Pour le backend, nous avons utilisé les technologies suivantes:

1. [NestJs](https://nestjs.com/) : Framework NodeJs écrit en TypeScript, utilisé pour implémenter notre backend (contrôleurs, routes, services, etc).
2. [MongoDB](https://www.mongodb.com/fr) : Base de données utilisée pour le stockage des données "légères". Le filesystem contient les grosses données.
3. [Libsodium.js](https://www.npmjs.com/package/libsodium) : Module NodeJs pour la création de clés publique / privée ainsi que pour signer des payloads lors de l'appairage.

## Ce qui fonctionne

1. L'authentification et l'enregistrement
    - Un utilisateur peut se créer un compte
    - Un utilisateur peut se loguer à son compte
2. Le téléchargement de vidéos sur le serveur
    *  Un utilisateur connecté peut uploader et downloader un clips vidéo
    *  Il peut récupérer la liste des vidéos enregistrées sur le serveur
    *  Il peut modifier les informations d'une vidéo
    *  Il peut supprimer une vidéo
3. Le système d'appairage (interactions avec le front-end)

## Ce qui ne fonctionne pas

* Le streaming
* L'appairage coté noeuds

## Quelques détails d'implémentation

### AuthGuard

NestJS permet de mettre en place un système de "Guard". Nous en avons mis un en place en suivant simplement les explications de la JWT Strategy disponibles [ici](https://docs.nestjs.com/techniques/authentication). Une fois le "guard" prêt, il suffit d'ajouter l'annotation `@UseGuards(AuthGuard())` avant un contrôleur ou une route spécifique pour forcer l'authentification.

Si l'utilisateur fait une requête sur un des endpoints "guardés" par le `AuthGuard` sans header d'autorisation (token donné lors de l'authentification), il recevra automatiquement un "Forbidden", avant même d'entrer dans le endpoint. 

Ce mécanisme est extrêmement pratique et facile à mettre en place car nous n'avons dès lors plus besoin de faire de vérification dans le code du endpoint!

### Class-Validator

NestJS permet également de travailler avec des DTO (Data Transfer Object). En effet, lorsqu'on fait un endpoint qui s'attend à certaines données, on peut créer une DTO pour récupérer les données sous forme d'objet. Dans cette DTO, on peut utiliser le `class-validator` , qui permet de vérifier le contenu d'un attribut à l'aide d'annotations. Plus de détails sur le `class-validator` sont disponibles [ici](https://docs.nestjs.com/techniques/validation).

Le `class-validator` vérifie le type des données qu'on nous transmet dans les requêtes ainsi que leur contenu. En utilisant des annotations, on peut rendre le champ obligatoire, ou bien obliger de recevoir une string, ou encore vérifier que cette string contient un email valide. 

Le `class-validator` offre énormément de possibilités! Si l'une des vérification échoue et donc que la requête ne correspond pas à ce qu'on attend, l'utilisateur recevra automatiquement un "Bad Request".

Ci-dessous, un exemple de DTO avec validateur pour l'authentification :

```typescript
import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail } from 'class-validator';

export class Auth {
  @ApiModelProperty()
  @IsNotEmpty()
  @IsEmail()
  readonly email: string;

  @ApiModelProperty()
  @IsNotEmpty()
  readonly password: string;
}
```

### Gestion du téléchargement de vidéos

Pour la gestion du téléchargement des vidéos, NestJs nous a considérablement surpris et facilité la tâche! En effet, si vous connectez votre base de données MongoDB à l'aide de l'intégration faite dans NestJs, il suffit d'utiliser un FileInterceptor comme paramètre de notre endpoint pour que NestJs prenne tout en charge!

```typescript
@UseInterceptors(FileInterceptor('file', { dest: './videos' }))
  async uploadVideo(@UploadedFile() file: Video) {
    return this.videosService.uploadVideo(file);
  }
```

Dès lors, NestJs s'occupe tout seul de télécharger le fichier, le stocker dans le filesystème etc! La seule chose qu'il nous faut encore faire, c'est enregistrer les métadatas dans la base de données!

### OpenAPI Doc

NestJS permet de générer une documentation OpenAPI (Swagger) à partir des annotations trouvées dans le code. Cela simplifie grandement l'implémentation et permet au front-end, d'accéder à `nomdedomain.xx/api` et de consulter ainsi la documentation de l'API automatiquement générée. Nous avons été attentifs à la maintenir à jour durant le développement du projet.

La documentation OpenAPI peut se faire assez simplement, il suffit de suivre la documentation [ici](https://docs.nestjs.com/recipes/swagger). Encore une fois, lorsque les outils sont installés, il suffit de quelques annotations dans les DTOs et dans les contrôleurs pour permettre à NestJS de générer une doc assez complète. Sur l'exemple de code du point précédent, on peut voir l'annotation `@ApiModelProperty()`, qui permet de dire à NestJS de mettre la propriété dans la documentation OpenAPI. Il est également possible de grouper les contrôleurs
ou endpoints dans des tags, etc.

Voici ce que donne la documentation une fois générée :

![alt text](openAPIExample.png)