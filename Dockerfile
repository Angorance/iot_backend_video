# Use latest node version
FROM node:latest

MAINTAINER Benoît Schopfer <benoit.schopfer@heig-vd.ch.ch>

# create backend directory in container
RUN mkdir -p /backend

# set /backend directory as default working directory
WORKDIR /backend

# only copy package.json initially so that it's recreated only if there are changes in package.json
ADD package.json ./

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied where available (npm@5+)
COPY package*.json ./

# Run npm install
RUN npm install

# copy all file from current dir to /backend in container
COPY . ./

# expose port 3030 and 27017
EXPOSE 3030 27017

# cmd to start service (run prod script of package.json)
CMD [ "npm", "run", "prod" ]
